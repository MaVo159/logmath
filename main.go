package logmath

import (
	"math"
)

//Log(1+x)
func Log1p(x float64) float64 {
	return math.Log1p(x)
}

//Log(Exp(a)+Exp(b))
func LogSum(a, b float64) float64 {
	b, a = sort(a, b)
	return a + Log1p(b-a)
}

//Log(Abs(Exp(a)-Exp(b)))
func LogDiff(a, b float64) float64 {
	b, a = sort(a, b)
	return Log1p(-math.Exp(b-a)) + a
}

func sort(a, b float64) (min, max float64) {
	if a > b {
		return b, a
	}
	return a, b
}
